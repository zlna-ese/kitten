/** @file HL_sys_main.c 
*   @brief Application main file
*   @date 11-Dec-2018
*   @version 04.07.01
*
*   This file contains an empty main function,
*   which can be used for the application.
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com  
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/* USER CODE BEGIN (0) */
/* USER CODE END */

/* Include Files */

#include "HL_sys_common.h"

/* USER CODE BEGIN (1) */
#include "FreeRTOS.h"
#include "os_task.h"
#include "os_timer.h"
#include "HL_het.h"
#include "HL_gio.h"
#include "HL_esm.h"
/* USER CODE END */

/** @fn void main(void)
*   @brief Application main function
*   @note This function is empty by default.
*
*   This function is called after startup.
*   The user can use this function to implement the application.
*/

/* USER CODE BEGIN (2) */
extern void _enable_interrupt_();
boolean bFast = true;
/* Define Task Handles */
xTaskHandle xTask1Handle;
xTaskHandle xTask2Handle;
uint32_t toggle = 1;
void vTaskHET(void *pvParameters)
{
  uint32_t ledly[8] = {0,5,10,15,25,30,70,0};
#define ON_OFF 3
#define ON     2
#define OFF_ON 1
#define OFF    0
  uint8_t b_pLedState = 1;  
  uint8_t b_cLedState = 0;
  
  uint8_t b_pBtnState = 1;
  uint8_t b_cBtnState = 1;
  uint8_t b_cBtnTemp = 1;
  
  uint8_t b_cSystemState = OFF;
  uint8_t b_pSystemState = OFF;
  
  uint8_t sample;
  
  pwmStop(hetRAM1, 0 );
  pwmStop(hetRAM1, 1 );
  pwmStop(hetRAM1, 2 );
  pwmStop(hetRAM1, 3 );
  pwmStop(hetRAM1, 4 );
  pwmStop(hetRAM1, 5 );
  pwmStop(hetRAM1, 6 );
  pwmStop(hetRAM1, 7 );
  
  while(1)
  {
    //Read Button  
    b_cBtnState = (uint8_t) gioGetBit(gioPORTA, 7);
    //Check for State Change
    if( b_pBtnState != b_cBtnState)	                               
    { 
      //save context of button;
      b_pBtnState = b_cBtnState;
      //Enforce consistent readings to debounce
      for(sample = 0; sample < 16;)                                     
      {
          if( ( b_cBtnTemp = (uint8_t) gioGetBit(gioPORTA, 7) ) == ( b_cBtnState ) )
          {
              sample++;
          }
          else
              sample = 0;
      }
      //state machine
      switch(b_cSystemState)
      {
          case OFF:
              b_cSystemState++;
              break;
          case OFF_ON:
              b_cSystemState++;
              gioSetBit(hetPORT1, 5,1);
              break;
          case ON:
              b_cSystemState++;
              break;
          case ON_OFF:
              b_cSystemState = OFF;
              gioSetBit(hetPORT1, 5,0);
              break;
          default:
              break;
        }
      }
    //Save System State
    b_pSystemState = b_cSystemState;
  }
}




/* USER CODE END */

uint8	emacAddress[6U] = 	{0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU};
uint32 	emacPhyAddress	=	1U;

int main(void)
{
  /* USER CODE BEGIN (3) */
  gioInit();
  hetInit();
  if (xTaskCreate(vTaskHET, (const signed char *)"TaskHET", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, &xTask1Handle) != pdTRUE)
  {
    /* Task could not be created */
    while(1);
  }
    vTaskStartScheduler();
    /* USER CODE END */
    
    return 0;
  
}
  /* USER CODE BEGIN (4) */
  /* USER CODE END */
  